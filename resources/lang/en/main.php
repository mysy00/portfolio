<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Main Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during displaying the main page
    |
    */
    'websiteTitle' => 'NoWitam',
    'mainTitle' => 'Hello',
    'article' => 'If you are looking for someone, who can code you a website - you have hit the perfect spot. Go to the "Contact" page as soon as possible and get in touch with me. I offer code of first-class quality, accordance with the latest standards. What does it mean? That you pay and have exactly what you need! You don\'t need to worry about anything. What I can do for you:
    <ul class="list">
        <li>Scripts and CMS installation and configuration</li>
        <li>Editing and making awesome themes and websites (Responsivity? First-mobile? Sure!)</li>
        <li>Various generators (e.g. graphic generator)</li>
        <li>Inormation (e.g. top lists) about your TeamSpeak, SA:MP, CS server on your website!</li>
    </ul>
    Contact me ASAP!',
];
