<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navigation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during displaying navigations over
    | the page 
    |
    */

    'homepage' => 'Start',
    'projects' => 'Projects',
    'contact' => 'Contact',

];
