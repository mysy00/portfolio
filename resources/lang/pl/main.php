<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Main Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during displaying the main page
    |
    */

    'websiteTitle' => 'NoWitam.pl || Divosky',
    'mainTitle' => 'No witam',
    'article' => 'Jeśli szukasz kogoś, kto wykona Ci stronę internetową lub styl świetnie trafiłeś. Przejdź jak najszybciej do zakładki "Kontakt" i skontaktuj się ze mną. Oferuje od siebie świetnej jakości kod, zgodny z najnowszymi standardami. Co to dla Ciebie oznacza? Że płacisz i masz to czego oczekujesz! Nie musisz się o nic martwić. Co mogę wykonać dla ciebie:
    <ul class="list">
        <li>Instalacja i konfiguracja skryptów, CMS</li>
        <li>Edycja i tworzenie wspaniałych styli i stron (Responsywność? First-mobile? Pewnie!)</li>
        <li>Różne generatory (np. grafiki)</li>
        <li>Informacje (np. top listy) twojego serwera TeamSpeak, samp, cs na stronie!</li>
    </ul>
    Skontaktuj się jak najszybciej!',

];
