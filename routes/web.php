<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return 'An error in loading the main page occured. :/';
});

Route::get('/', 'PageController@start');
Route::get('/projects', 'PageController@projects');
Route::get('/contact', 'PageController@contact');
